const fetch = require('node-fetch');
const fs = require('fs');
const timestamp = require('time-stamp');

function actualizarFuncion() {
    const status = response => {
        if (response.status >= 200 && response.status < 300) {
            return Promise.resolve(response);
        }

        return Promise.reject(new Error(response.statusText));
    };
    const obtenerJson = response => {
        return response.json();
    }

    fetch('https://mindicador.cl/api')
        .then(status)
        .then(obtenerJson)
        .then((datos) => {
            fs.writeFile('./librerias/' + timestamp('YYYYMMDD') + '.json', JSON.stringify(datos), (err) => { console.error(err) });
        })
        .catch(console.error);

}

module.exports = actualizarFuncion;