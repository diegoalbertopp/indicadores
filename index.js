const fs = require('fs');
const fetch = require('node-fetch');
const readline = require('readline');
var actualizarFuncion = require('./librerias/dataAPI.js');

var lector = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

menu();

function menu() {
    console.log('Menu');
    console.log('1. Actualizar datos');
    console.log('2. Promedio últimos cinco archivos');
    console.log('3. Mínimo y máximo últimos cinco archivos');
    console.log('4. Salir');
    lector.question('Escriba su respuesta: ', opcion => {
        switch (opcion) {
            case '1':
                console.log('Opción 1');
                actualizarFuncion();
                menu();
                break;
            case '2':
                console.log('Opción 2');
                menu();
                break;
            case '3':
                console.log('Opción 3');
                menu();
                break;
            case '4':
                console.log('Adios');
                lector.close();
                break;
            default:
                console.log('Opción ingresada no es válida, intente nuevamnete');
                menu();
        }
    });

}